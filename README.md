# Virtual Reality Spider Simulator #

For the gamer who wants to be a spider, the �VR Spider Simulator�
is a virtual reality game that allows the player to finally live
out their lifelong dream of being a spider. Being a spider is not
easy; you will have to catch food to eat in order to survive.
This game will also feature randomly generated levels so
the game is different every time you play. Unlike other spider simulator games,
our game uses google cardboard to create an immersive, first-person,
virtual reality experience that will put you in the spider seat.

This was my short term individual senior project in college.
I was learning Unity and Android deployment as I went.

### Features ###

* Be a spider
* Use webs to capture bugs to eat in order to survive
* Control spider with handheld controller and Google cardboard head tracking
* Randomly generated levels

### Built with Unity 4.7 ###

* Prototype as of June 2016

### Author ###

* Kevin Hutt

## Acknowlegements

Tutorial Script for MOGA Unity Android/WP8
YouTube Tutorial Link: http://youtu.be/YryG2hoeLsI
�2013 Bensussen Deutsch and Associates, Inc. All rights reserved.

Procedual Cave Generation tutorial by Sebastion Lague
https://unity3d.com/learn/tutorials/projects/procedural-cave-generation-tutorial