﻿// class for using Moga controller in the game over menu

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Input = Moga_Input;
using UnityEngine.SceneManagement;

public class MenuEvents : MonoBehaviour {

	private KeyCode startButtonKeyCode,
		selectButtonKeyCode,
		aButtonKeyCode;
	private GameObject mogaManagerObject;
	private Moga_ControllerManager mogaManagerScript;
	bool mogaFound = false;

	// Use this for initialization
	void Start () {
	
		// Try Find our Moga Manager Game Object
		mogaManagerObject = GameObject.Find("MogaControllerManager");

		// If it exists..
		if (mogaManagerObject != null) {
			// Check the Moga Manager Script is correctly attached to the Moga  Manager Game Object
			mogaManagerScript = mogaManagerObject.GetComponent<Moga_ControllerManager> ();

			// If it is attached...
			if (mogaManagerScript != null) {
				// Register MOGA Controller
				Input.RegisterMogaController ();

				// Get our mapped KeyCode Values and assign them.
				aButtonKeyCode = mogaManagerScript.p1ButtonA;
				startButtonKeyCode = mogaManagerScript.p1ButtonStart;
				selectButtonKeyCode = mogaManagerScript.p1ButtonSelect;
				mogaFound = true;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {

		// start to reset, select to exit
		if (mogaFound) {
			if(Input.GetKeyDown(startButtonKeyCode) ){
				restart ();
			}
			if (Input.GetKeyDown (selectButtonKeyCode)) {
				exit ();
			}
		}
	}

	public void exit()
	{
		Application.Quit();
	}
	public void restart()
	{
		SceneManager.LoadScene (0);
	}
}
