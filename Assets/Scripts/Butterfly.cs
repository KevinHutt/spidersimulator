﻿using UnityEngine;
using System.Collections;

public class Butterfly : MonoBehaviour {

	public Rigidbody rb;
	public float time;
	public int speed;
	public bool stuck;
	private Animation anim;

	// Use this for initialization
	void Start() {
		stuck = false;
		time = Random.Range(0, 100);
		anim = GetComponent<Animation>();
		rb = GetComponent<Rigidbody>();
		moveRandom();
	}
	
	// Update is called once per frame
	void Update() {

		// move in a random direction after a random amount of time
		if (time < 0 && !stuck) {
			moveRandom();
			time = Random.Range(0, 100);
		}
		time--;
	}

	// move in a random direction
	void  moveRandom() {

		Vector3 randomDirection = Random.insideUnitSphere * speed;
		transform.eulerAngles = new Vector3(randomDirection.x, 0, randomDirection.z);
		rb.velocity = randomDirection;
	}

	// get stuck on collision with web
	void OnCollisionEnter(Collision col) {
		if (col.gameObject.name.StartsWith("Web")) {
			stuck = false;
			this.tag = "Bug";
			rb.constraints = RigidbodyConstraints.FreezeAll;
			anim.Stop();
		}
	}
}
