﻿// Controller class to oversee all butterflies

using UnityEngine;
using System.Collections;
using System;

public class ButterflyController : MonoBehaviour {

	public int maxButterflies;
	private int nButterflies;
	public GameObject butterfly;
	public int minX, maxX, minZ, maxZ;

	// Use this for initialization
	void Start() {
		// create butterflies until reaching max
		for (int i = 0; i < maxButterflies; i++) {
			createButterfly(i);
		}
	}
	
	// Update is called once per frame
	void Update() {
	
		// create a butterfly if any are missing
		if (nButterflies < maxButterflies) {
			createButterfly(1);
		}
	}

	// creates a new butterfly at a random location
	void createButterfly(int i) {
		int seed = i * (int)DateTime.Now.Ticks;
		System.Random r = new System.Random(seed);
		float x = r.Next(minX, maxX);
		float z = r.Next(minZ, maxZ);
		Vector3 pos = new Vector3(x, -5f, z);
		GameObject clone = Instantiate(butterfly, pos, Quaternion.identity) as GameObject;
		clone.transform.parent = transform;
		nButterflies++;
	}

	public void decrement() {
		nButterflies--;
	}
}
