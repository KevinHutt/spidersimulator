﻿// Controller class for the player (spider)
// Uses some code from:
// Tutorial Script for MOGA Unity Android/WP8
// YouTube Tutorial Link: http://youtu.be/YryG2hoeLsI
// ©2013 Bensussen Deutsch and Associates, Inc. All rights reserved.
// Adapted and added to for educational use by Kevin Hutt 2016

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Extends Unitys inbuilt InputManager with our custom one (Moga_Input)
using Input = Moga_Input;
using UnityEngine.SceneManagement;

public class HeadController : MonoBehaviour {

	// for moga controller
	private KeyCode aButtonKeyCode,
		bButtonKeyCode,
		xButtonKeyCode,
		yButtonKeyCode,
		startButtonKeyCode,
		selectButtonKeyCode,
		L2ButtonKeyCode;
	private GameObject mogaManagerObject;
	private Moga_ControllerManager mogaManagerScript;
	bool mogaFound = false;

	// for Cardboard VR
	public GameObject[] cardboardObjects;
	public GameObject[] monoObjects;
	public Cardboard cardboard;

	// basic properties
	private Rigidbody rb;
	public float speed;
	public Animation anim;
	private GameObject surface;
	private Vector3 startPos;
	private Quaternion startRot;

	// for wall walking
	private bool wallWalking;
	private bool isGrounded;
	private bool onWall;
	private bool onCeiling;

	// object interactions
	public Transform web;
	public ButterflyController butterflyController;

	// hunger guage stuff
	float barDisplay = 1f;
	Vector2 pos = new Vector2(50,100);
	Vector2 size = new Vector2(600,200);
	public Texture2D progressBarEmpty;
	public Texture2D progressBarFull;


	// Use this for initialization
	void Start() {

		// initialize variables
		isGrounded = true;
		onWall = false;
		onCeiling = false;
		anim = GetComponent<Animation>();
		rb = GetComponent<Rigidbody>();

		// Try Find our Moga Manager Game Object
		mogaManagerObject = GameObject.Find("MogaControllerManager");

		// If it exists..
		if (mogaManagerObject != null) {
			// Check the Moga Manager Script is correctly attached to the Moga  Manager Game Object
			mogaManagerScript = mogaManagerObject.GetComponent<Moga_ControllerManager>();

			// If it is attached...
			if (mogaManagerScript != null) {
				// Register MOGA Controller
				Input.RegisterMogaController();

				// Get our mapped KeyCode Values and assign them.
				aButtonKeyCode = mogaManagerScript.p1ButtonA;
				bButtonKeyCode = mogaManagerScript.p1ButtonB;
				xButtonKeyCode = mogaManagerScript.p1ButtonX;
				yButtonKeyCode = mogaManagerScript.p1ButtonY;
				startButtonKeyCode = mogaManagerScript.p1ButtonStart;
				selectButtonKeyCode = mogaManagerScript.p1ButtonSelect;
				L2ButtonKeyCode = mogaManagerScript.p1ButtonL2;
				mogaFound = true;
			}
		}

		// get starting position and rotation
		startPos = transform.position;
		startRot = transform.rotation;

		Switch(); // enable VR

	}

	// Update is called once per frame
	void Update() {
		
		if (mogaFound) {
			
			// movement and camera rotation
			this.transform.Rotate(0, Input.GetAxis("LookHorizontal") * 3, 0);
			float moveHorizontal = Input.GetAxis("Horizontal");
			float moveVertical = -Input.GetAxis("Vertical");
			Vector2 input = new Vector2(moveHorizontal, moveVertical);
			Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);
			transform.Translate(movement * speed, transform);

			// walking animation
			if (moveHorizontal != 0 || moveVertical != 0 || Input.GetAxis("LookHorizontal") != 0) {
				anim.Play("walk");
			} else {
				anim.Play("idle");
			}
				
			// wall walking
			if (Input.GetKey(L2ButtonKeyCode) || Input.GetKey("space")) {
				wallWalking = true;
				rb.useGravity = false;
				rb.AddRelativeForce(0, -10, 0);
			} else {
				wallWalking = false;
				rb.useGravity = true;
			}
		}

		// switch VR mode with select button
		if (Input.GetKeyDown(selectButtonKeyCode) || Input.GetKeyDown("enter")) {
			Switch();
		}

		// spin a web with B button
		if (Input.GetKeyDown(bButtonKeyCode) || Input.GetMouseButtonDown(0)) {
			Vector3 pos = transform.position + transform.forward * 2 + transform.up * 2;
			Instantiate(web, pos, transform.rotation);
		}

		// reset spider position and rotation with start button
		if (Input.GetKeyDown(startButtonKeyCode)) {
			transform.position = startPos;
			transform.rotation = startRot;
			cardboard.Recenter ();
		}

		// jump
		if (Input.GetKeyDown(aButtonKeyCode)) {
			rb.AddRelativeForce(0, 250, 0);
		}
			
		// reorent if free falling
		if ((rb.velocity.y > 1) && !wallWalking) {
			reOrient();
		}

		// eat a bug with X button if close enough
		if ((Input.GetKeyDown(xButtonKeyCode)) || Input.GetMouseButtonDown(1)) {
			GameObject bug = FindClosestBug();

			// calculate distance to bug
			Vector3 directionToTarget = transform.position - bug.transform.position;
			float angle = Vector3.Angle(transform.forward, directionToTarget);
			float distance = directionToTarget.sqrMagnitude;

			if (Mathf.Abs(angle) < 180 && distance < 2) {
				Destroy(bug);
				butterflyController.decrement();
				barDisplay += 0.5f;
			}
		}

		// eat a web with Y button if close enough
		if ((Input.GetKeyDown(yButtonKeyCode))) {
			GameObject web = FindClosestWeb();

			// calculate distance to web
			Vector3 directionToTarget = transform.position - web.transform.position;
			float angle = Vector3.Angle(transform.forward, directionToTarget);
			float distance = directionToTarget.magnitude;

			if (Mathf.Abs(angle) < 180 && distance < 25) {
				Destroy(web);
			}
		}

		// lose hunger over time
		barDisplay -= 0.0001f;

		// load game over scene if hunger guage reaches zero
		if (barDisplay <= 0) {
			SceneManager.LoadScene (1);
		}

	} // end Update
		
	GameObject FindClosestBug() {
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Bug");
		GameObject closest = null;
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance) {
				closest = go;
				distance = curDistance;
			}
		}
		return closest;
	}

	GameObject FindClosestWeb() {
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Web");
		GameObject closest = null;
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance) {
				closest = go;
				distance = curDistance;
			}
		}
		return closest;
	}
		
	// detects collision with surfaces and
	// rotates the player as needed to transition
	// from surface to surface, i.e. from floor to wall or vis versa
	void OnCollisionEnter(Collision col) {

		if (col.gameObject.name.StartsWith("Floor")) {
			isGrounded = true;
			onCeiling = false;

			if (onWall) { // transition from wall to floor
				transform.Rotate(-90F, 0, 0);
				Vector3 temp = new Vector3(0, 1.0F, 0);
				transform.position += temp;
				onWall = false;
			}
		}
		if ((col.gameObject.name.StartsWith("Wall") || col.gameObject.name.StartsWith("Web")) && wallWalking) {
			
			if (isGrounded) { // transition from ground to wall
				transform.Rotate(-90F, 0, 0);
				Vector3 temp = new Vector3(0, 1.0F, 0);
				transform.position += temp;
				isGrounded = false;
				onWall = true;
			}

			if (onCeiling) { // transition from ceiling to wall
				transform.Rotate(-90F, 0, 0);
				Vector3 temp = new Vector3(0, -1.0F, 0);
				transform.position += temp;
				onWall = true;
				onCeiling = false;
			}
		}

		if (col.gameObject.name.StartsWith("Ceiling")) { // transition onto ceiling
			if (!onCeiling) {
				transform.Rotate(-90, 0, 0);
				Vector3 temp = new Vector3(1, 0, 0);
				transform.position += temp;
				onCeiling = true;
				onWall = false;
			}
		}
	}
		
	void OnCollisonExit(Collision col) {
		// do nothing
	}

	// rotates the player to upright position
	void reOrient() {
		transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
	}

	// Turn on or off VR mode
	void ActivateVRMode(bool goToVR) {
		foreach (GameObject cardboardThing in cardboardObjects) {
			cardboardThing.SetActive(goToVR);
		}
		foreach (GameObject monoThing in monoObjects) {
			monoThing.SetActive(!goToVR);
		}
		Cardboard.SDK.VRModeEnabled = goToVR;
	}

	// switch BaseVRDevice mode
	public void Switch() {
		ActivateVRMode(!Cardboard.SDK.VRModeEnabled);
	}
}
