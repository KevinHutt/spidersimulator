﻿using UnityEngine;
using System.Collections;

public class AnimationSpeed : MonoBehaviour {

	public Animation anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animation> ();
		foreach (AnimationState state in anim) {
			state.speed = 0.1F;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
